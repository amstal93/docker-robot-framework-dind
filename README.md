# jfxs / robot-framework-dind

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Pipeline Status](https://gitlab.com/fxs/docker-robot-framework-dind/badges/master/pipeline.svg)](https://gitlab.com/fxs/docker-robot-framework-dind/pipelines)
[![Robot Framework Tests](https://fxs.gitlab.io/docker-robot-framework-dind/all_tests.svg)](https://fxs.gitlab.io/docker-robot-framework-dind/report.html)
[![Image size](https://fxs.gitlab.io/docker-robot-framework-dind/docker.svg)](https://hub.docker.com/r/jfxs/robot-framework-dind)

A lightweight automatically updated and tested multiarch amd64 and arm64 Docker image to run [Robot Framework](https://robotframework.org/) to test Docker images with [Docker in Docker](https://hub.docker.com/_/docker).

## Getting Started

### Prerequisities

In order to run this container you'll need Docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

Example to run Robot Framework tests in your current directory:

```shell
docker run -it --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/reports:/reports jfxs/robot-framework-dind robot --outputdir /reports /tests
```

## Built with

Docker latest tag contains:

See versions on [Dockerhub](https://hub.docker.com/r/jfxs/robot-framework-dind)

Versions of installed software are listed in /etc/VERSIONS file of the Docker image. Example to see them for a specific tag:

```shell
docker run -t jfxs/robot-framework-dind:3.1.2-1 cat /etc/VERSIONS
```

mask.sh is a shell program to mask confidential variables in files, especially in output.xml Robot Framework file before publishing.

```shell
Usage: mask.sh -f <file_path> -m <variable1>,<variable2>
Options:
  -f file path of the log file
  -m list of variables to mask separated by comma
  -v | --version print version
  -h print usage

Example: mask.sh -f output.xml -m myPassword
```

## Versioning

The Docker tag is defined by the Robot Framework used version and an increment to differentiate build with the same Robot Framework version:

```text
<robotframework_version>-<increment>
```

Example: 3.1.2-1

## Tests

Tests are runned with [Robot Framework](http://robotframework.org/).
Last test report is available [here](https://fxs.gitlab.io/docker-robot-framework-dind/report.html).

## Vulnerability Scan

The Docker image is scanned every day with the open source vulnerability scanner [Trivy](https://github.com/aquasecurity/trivy).

The latest vulnerability scan report is available on [Gitlab Security Dashboard](https://gitlab.com/fxs/docker-robot-framework-dind/-/security/dashboard/?state=DETECTED&state=CONFIRMED&reportType=CONTAINER_SCANNING).

## Find Us

* [Dockerhub](https://hub.docker.com/r/jfxs/robot-framework-dind)
* [Gitlab](https://gitlab.com/fxs/docker-robot-framework-dind)

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/docker-robot-framework-dind/blob/master/LICENSE) file for details.
