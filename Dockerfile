# hadolint ignore=DL3006
ARG IMAGE_FROM_SHA
# hadolint ignore=DL3007
FROM jfxs/robot-framework:latest as robot-framework
# hadolint ignore=DL3007
FROM jfxs/ci-toolkit:latest as ci-toolkit

# hadolint ignore=DL3006
FROM ${IMAGE_FROM_SHA}

ARG IMAGE_FROM_SHA
ARG RF_VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="robot-framework-dind" \
    org.opencontainers.image.description="A lightweight Docker image to run Robot Framework tests" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="GPL-3.0-or-later" \
    org.opencontainers.image.version="${RF_VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/robot-framework-dind" \
    org.opencontainers.image.source="https://gitlab.com/fxs/docker-robot-framework-dind" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY --from=robot-framework /usr/local/bin/mask.sh /usr/local/bin/mask.sh
COPY --from=ci-toolkit /usr/local/bin/get-local-versions.sh /usr/local/bin/get-local-versions.sh

# hadolint ignore=DL3018,DL3013
RUN apk --no-cache add \
        make \
        python3 \
        py3-pip \
 && apk --no-cache add --virtual \
        .build-deps \
        build-base \
        python3-dev \
 && pip3 install --no-cache-dir --upgrade \
        pip \
 && pip3 install --no-cache-dir \
        robotframework==${RF_VERSION} \
        robotframework-requests \
 && /usr/local/bin/get-local-versions.sh -f ${IMAGE_FROM_SHA} -a python3 -c docker,mask.sh -p robotframework,robotframework-requests \
 && apk --no-cache del .build-deps

# hadolint ignore=DL3059
RUN addgroup nobody root && \
    mkdir -p /tests /reports && \
    chgrp -R 0 /reports && \
    chmod -R g=u /etc/passwd /reports

WORKDIR /tests

CMD ["robot", "--version"]
